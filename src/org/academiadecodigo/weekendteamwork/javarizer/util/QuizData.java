package org.academiadecodigo.weekendteamwork.javarizer.util;

public class QuizData {

    /**
     * fields
     */
    public String question;
    public String a1;
    public String a2;
    public String a3;
    public String a4;
    public String correct;

    /**
     * constructor
     * @param question
     * @param a1
     * @param a2
     * @param a3
     * @param a4
     * @param correct
     */
    public QuizData(String question, String a1, String a2, String a3, String a4, String correct){

        this.question = question;
        this.a1 = a1;
        this.a2 = a2;
        this.a3 = a3;
        this.a4 = a4;
        this.correct = correct;
    }
}
