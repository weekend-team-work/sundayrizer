package org.academiadecodigo.weekendteamwork.javarizer;

import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import org.academiadecodigo.weekendteamwork.javarizer.util.Color;
import org.academiadecodigo.weekendteamwork.javarizer.util.QuizData;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class QuizRound implements Runnable {

    /**
     * fields
     */
    private final String beforeMenu;

    private Player player;
    private Server server;

    private LinkedList<QuizData> listQuiz;
    private LinkedList<String> winners;


    /**
     * constructor
     *
     * @param player
     * @param server
     */
    public QuizRound(Player player, Server server) {

        this.player = player;
        this.server = server;
        listQuiz = new LinkedList<>();
        winners = new LinkedList<>();
        beforeMenu = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
    }

    public void getQuestions() {

        listQuiz.add(new QuizData(
                "QUESTION 1/6 - What is a correct syntax to output \"Hello World\" in Java?",
                "System.out.println(\"Hello World\");",
                "print (\"Hello World\");",
                "Console.WriteLine(\"Hello World\");",
                "echo(\"Hello World\");",
                "1-System.out.println(\"Hello World\");"));
        listQuiz.add(new QuizData("QUESTION 2/6 - Which of these allows duplicate elements?",
                "Set", "List", "All", "None of the above", "2-List"));
        listQuiz.add(new QuizData("QUESTION 3/6 - What is a constructor?",
                "Another name for an instance variable",
                "The return type of a method",
                "The method that creates an object, or instance, of the class",
                "The instantiation of an object",
                "3-The method that creates an object, or instance, of the class"));
        listQuiz.add(new QuizData("QUESTION 4/6 - What guarantees type-safety in a collection?",
                "Generics",
                "Abstract classes",
                "Interfaces",
                "Collection",
                "1-Generics"));
        listQuiz.add(new QuizData("QUESTION 5/6 - In iterator, how do you get the next element?",
                "next()",
                "getNext()",
                "returnNext()",
                "name remains same",
                "1-next()"));
        listQuiz.add(new QuizData("QUESTION 6/6 - How many threads can be executed at a time?",
                "Only one thread",
                "Multiple threads",
                "Only main (main() method) thread",
                "Two threads",
                "2-Multiple threads"));
    }

    public void round() {

        int counter = 0;

        for (QuizData quiz : listQuiz) {

            counter++;

            String[] menu = {quiz.a1, quiz.a2, quiz.a3, quiz.a4};

            MenuInputScanner menuInputScanner = new MenuInputScanner(menu);
            menuInputScanner.setMessage(Color.GREEN + quiz.question + Color.WHITE);

            // getting the correct answer number
            int numberCorrectAnswer = Integer.parseInt(quiz.correct.split("-")[0]);
            // getting the correct answer description
            String correctAnswer = quiz.correct.split("-")[1];

            player.getOut().println(Color.CYAN + beforeMenu + Color.WHITE);
            int playersAnswer = player.getPrompt().getUserInput(menuInputScanner);

            // if answer is correctAnswer, increase score
            if (playersAnswer == numberCorrectAnswer) {
                player.incrementScore();
            }

            player.getOut().println('\n' + Color.MAGENTA + ">> Correct answer: " + correctAnswer + '\n' + Color.WHITE);

            if (counter == listQuiz.size()) {
                player.setFinished(true);

                drawingGameOver();
            }
        }

        getResults();
        displayResults();
    }

    @Override
    public void run() {
        getQuestions();
        round();

        server.broadcast("\n** Ate ja, Luis! **\n");
        System.exit(1);
    }

    public void drawingGameOver() {

        player.getOut().println("\n                        .-\"\"\"-.\n" +
                "                       / .===. \\\n" +
                "                       \\/ 6 6 \\/\n" +
                "                       ( \\___/ )\n" +
                "  _________________ooo__\\_____/_____________________\n" +
                " /                                                  \\\n" +
                "|            Quiz Over... Waiting for results!       |\n" +
                " \\______________________________ooo_________________/\n" +
                "                       |  |  |\n" +
                "                       |_ | _|\n" +
                "                       |  |  |\n" +
                "                       |__|__|\n" +
                "                       /-'Y'-\\\n" +
                "                      (__/ \\__)\n\n");
    }

    public void getResults() {

        for (Player player : server.getPlayersList()) {
            if (!player.isFinished()) {
                getResults();
            }
        }
    }

    public void displayResults() {

        int maxPoints = 0;

        for (Player player : server.getPlayersList()) {
            if (player.getScore() > maxPoints) {
                maxPoints = player.getScore();
            }
        }

        for (Player player : server.getPlayersList()) {
            if (player.getScore() == maxPoints) {
                winners.add(player.getUsername());
            }
        }

        for (Player player : server.getPlayersList()) {
            server.broadcast(player.getUsername().toUpperCase() + ": " +
                    player.getScore() + " correct answers.");
        }

        displayWinners();

    }

    private void displayWinners() {
        server.broadcast(Color.CYAN + "\n======================================================\n");

        if (winners.size() > 1) {

            server.broadcast(Color.GREEN + "And we've got a tie between our players:\n" + Color.MAGENTA);

            for (String names : winners) {
                server.broadcast(names.substring(0, 1).toUpperCase() + names.substring(1));
            }

            server.broadcast(Color.WHITE + "\nTry again! You can do it!");

        } else {
            server.broadcast(Color.GREEN + "And the winner is...\n");
            server.broadcast(Color.MAGENTA + winners.getFirst());
            server.broadcast(Color.WHITE + "\nCongratulations!");
        }
    }

}
